const { getJSON } = require('./index');
const fetchMock = require('node-fetch');

const defaultResponse = [{
	test: 'true',
}]

beforeEach(() => {
	fetchMock.resetThrow();
	fetchMock.override(defaultResponse);
});

describe('getJSON utility function...', () => {

	test('Returns valid JSON data returned from fetch', async (done) => {
		const url = 'https://www.google.com';
		const result = await getJSON(url);
		expect(fetchMock).toHaveBeenCalledWith(url);
		expect(result).toEqual(defaultResponse);
		done();
	});

	test('Returns null data if fetch throws an error', async (done) => {
		fetchMock.setToThrow();
		const url = 'some bad url';
		const result = await getJSON(url);
		expect(fetchMock).toHaveBeenCalledWith(url);
		expect(result).not.toEqual(defaultResponse);
		expect(result).toBeNull();
		done();
	});

});