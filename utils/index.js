const fetch = require('node-fetch');

/**
 * Wrapper around fetch.  Performs a get on
 * given url and returns either a valid JSON
 * response from the fetch or null if an error 
 * is thrown.
 * 
 * @function getJSON
 * @param {String} url 
 * @returns {Object}
 */
const getJSON = async (url) => {
	try {
		const result = await fetch(url);
		const json = await result.json();
		return json; 
	} catch (error) {
		return null;
	}
};

module.exports = {
	getJSON,
};