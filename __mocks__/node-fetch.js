let returnData = [{
	test: 'true',
}]

let error = false;

const fetch = jest.fn(async (url) => {
	if (!error) {
		return {
			json: jest.fn(async () => returnData),
		}
	}
	throw new Error('Fetch Error!');
});

fetch.override = (mockData) => {
	returnData = mockData;
}

fetch.setToThrow = () => {
	error = true;
}

fetch.resetThrow = () => {
	error = false;
}

module.exports = fetch;