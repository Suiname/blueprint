# Blueprint Code Challenge

## Installation Instructions
I built this using node v10.15.3.  The easiest way to run this is to use a linux or unix shell with [node version manager.](https://github.com/creationix/nvm#installation-and-update)
Once node version manager is installed, you can run my application with the commands:
```
nvm install  # install node version
nvm use      # set nvm to use specific node version
npm install  # install node dependencies
npm start    # start project
```
This will start the webserver on port 3000.  If you want to run the unit tests, this can be done with the commands:
```
npm test
```

## Problem Description 
A client wants to be able to retrieve data from 3 websites simultaneously.  However, the responses from these websites varies in both request fulfillment time and reliability.  I created a simple node-express application that makes these requests asynchronously and aggregates the data, sending a JSON object to the client as soon as all 3 downstream websites respond.

## Technical Decisions
I chose to use [Node](https://nodejs.org/en/) and [Express](http://expressjs.com/) for a few reasons:

1.  IMO, JavaScript is the easiest language to use for manipulating and aggregating JSON
2.  Node is single-threaded and asynchronous by default, so a task of retrieving 3 requests asynchronously is one this tool was built for
3.  Deciding to use the fetch library to make the 3 requests in parallel minimizes the overall request time
4.  Node is very lightweight, good for a simple solution like this
5.  I chose to use the async / await design pattern instead of promises or callbacks because I find it more readable and easier to understand
6.  Express runs on port 3000 by default 😉

## Solution Design Decisions
The biggest decision that needed to be made is probably about how to handle the response in the event that one of the 3 websites sends back a failed request and/or improperly formatted JSON.  Since the requirements don't say what should be done in that case, I made what I thought are some reasonable assumptions.  I decided to always return a JSON response with any data received from each downstream request.  If any of the requests failed, I would set the HTTP status code to 502, which is indicative of a downstream failure, and make the corresponding value in the response JSON an empty array.  I thought this decision is the most flexible for the client, giving them all the data in a predictable format but still giving an indicator that all the data from downstream is not valid.

If I had more time I would implement a better way to handle failures, best to get the input from the client on how they want it presented on their side.  One of the flaws in my current implementation is the status code is just a catch all indicating a failure, the client might want to know which service specifically failed and more data about the failure.  I wrote a simple unit test for my fetch utility, but if I had more time I would write tests for the express router logic.  That involves using superagent or some other more complicated mock, which would definitely put me over the time limit so I didn't try it.

## Personal info
[Here's my linkedin profile](https://www.linkedin.com/in/jason-tham-12697010a/).  You should be able to find all relevant info about me there.