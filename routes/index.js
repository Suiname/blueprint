const express = require('express');
const router = express.Router();
const fetch = require('node-fetch');
const { getJSON } = require('../utils');

router.get('/', async (_req, res, _next) => {
  /**
   * Default route.  Asynchronously performs fetch GET requests
   * to the twitter, facebook, and instagram URLs and returns
   * the responses in a JSON object.  If any of the requests
   * error out, a JSON object is still served, but the status
   * code is set too 502 and the corresponding data for that url
   * is set to an empty array.
   */
  const twitter = getJSON('https://takehome.io/twitter');
  const facebook = getJSON('https://takehome.io/facebook');
  const instagram = getJSON('https://takehome.io/instagram');
  const result = {
    twitter: await twitter || [],
    facebook: await facebook || [],
    instagram: await instagram || [],
  };
  const errorFree = await (twitter && facebook && instagram);
  const status = !!errorFree ? 200 : 502;
  res.status(status).json(result);
});

module.exports = router;
